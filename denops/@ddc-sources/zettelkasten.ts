import {
    BaseSource,
    Item,
} from "https://deno.land/x/ddc_vim@v3.0.0/types.ts";
import {
    GatherArguments,
} from "https://deno.land/x/ddc_vim@v3.0.0/base/source.ts"



type Params = {
    zettelpath: string;
};


export class Source extends BaseSource<Params> {
    async gather(
        args: GatherArguments<Params>,
    ): Promise<Item> {

        const p = args.sourceParams;
        const zettelPath = p.zettelpath;
        const files = await Deno.readDir(zettelPath);
        const fileNames = [];
        for await (const dirEntry of files) {
            if (dirEntry.isFile) {
                fileNames.push(dirEntry.name);
            }
        }
        const result: Item[] = fileNames.map((candidate): Item => ({ word: candidate }));
        return result
    }

    params(): Params {
        return {
            zettelpath: "",
        };
    }
}
